const fs = require("fs").promises;

const getBoardInformation = require("./problem1.js");
const getListInformation = require("./problem2.js");
const getCardsInformation = require("./problem3.js");

function problem4() {
  let boardId;

  // Read the boards file
  return fs
    .readFile("../boards_1.json", "utf-8")
    .then((data) => {
      const allBoards = JSON.parse(data);

      // Find the board with name 'Thanos'
      const thanosBoard = allBoards.find((board) => board.name === "Thanos");

      if (!thanosBoard) {
        throw new Error("Board 'Thanos' not found");
      }

      boardId = thanosBoard.id;

      // Get and print board information
      return getBoardInformation(boardId);
    })
    .then((board) => {
      console.log(board);

      // Get and print lists information
      return getListInformation(boardId);
    })
    .then((lists) => {
      console.log(lists);

      // Find the list with name 'Mind'
      const mindList = lists.find((list) => list.name === "Mind");

      if (!mindList) {
        throw new Error("List 'Mind' not found");
      }

      // Get and print cards information for the 'Mind' list
      return getCardsInformation(mindList.id);
    })
    .then((cards) => {
      console.log(cards);
    })
    .catch((error) => {
      console.error(error);
    });
}

module.exports = problem4;
