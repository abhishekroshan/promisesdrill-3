const fs = require("fs").promises;

const getBoardInformation = require("./problem1.js");
const getListInformation = require("./problem2.js");
const getCardsInformation = require("./problem3.js");

function problem6() {
  let boardId;

  // Read the boards file
  return fs
    .readFile("../boards_1.json", "utf-8")
    .then((data) => {
      const allBoards = JSON.parse(data);

      // Find the board with name 'Thanos'
      const thanosBoard = allBoards.find((board) => board.name === "Thanos");

      if (!thanosBoard) {
        throw new Error("Board 'Thanos' not found");
      }

      boardId = thanosBoard.id;

      // Print board information
      console.log(thanosBoard);

      // Get and print board information
      return getBoardInformation(boardId);
    })
    .then(() => getListInformation(boardId))
    .then((lists) => {
      // Print lists information
      console.log(lists);

      // Collect promises for getting cards for each list
      const allLists = lists.map((element) => {
        return getCardsInformation(element.id);
      });

      return Promise.all(allLists);
    })
    .then((cards) => {
      console.log(cards);
    })
    .catch((error) => {
      console.error(error);
    });
}

module.exports = problem6;
