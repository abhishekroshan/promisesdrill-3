const fs = require("fs").promises;

function getBoardInformation(boardId) {
  return fs
    .readFile("../boards_1.json", "utf-8")
    .then((data) => {
      const boards = JSON.parse(data);
      // Parse the JSON data from the file

      const result = boards.find((element) => {
        //using the find method to find the element with the specific Id
        if (element.id === boardId) {
          //If the element id matches we return the element
          return element;
        }
      });
      return result;
    })
    .catch((error) => {
      console.log(error);
      // Handle any errors that occur during file reading or JSON parsing
    });
}

module.exports = getBoardInformation;
