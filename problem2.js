const fs = require("fs").promises;

function getListInformation(boardId) {
  return fs
    .readFile("../lists_1.json", "utf8")
    .then((data) => {
      const allLists = JSON.parse(data);
      // Parse the JSON data from the file

      for (const key in allLists) {
        // Iterating through each key in the object
        if (key === boardId) {
          // Checking if the current key matches the specified boardId
          return allLists[key];
          // Returning the value associated with the matching key
        }
      }
    })
    .catch((error) => {
      console.log(error);
      // for handling any errors that occur during file reading or JSON parsing
    });
}

module.exports = getListInformation;
