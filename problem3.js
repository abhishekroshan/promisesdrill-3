const fs = require("fs").promises;

function getCardsInformation(boardId) {
  return fs
    .readFile("../cards_1.json", "utf8")
    .then((data) => {
      const allCards = JSON.parse(data);
      // Parse the JSON data from the file

      for (const key in allCards) {
        // Iterating through each key in the object
        if (key === boardId) {
          // Checking if the current key matches the specified id
          return allCards[key];
          // Returning the value associated with the matching key
        }
      }
    })
    .catch((error) => {
      console.log(error);
      // for handling any errors that occur during file reading or JSON parsing
    });
}

module.exports = getCardsInformation;
